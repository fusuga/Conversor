﻿


namespace Lands.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using System.Windows.Input;
    using Xamarin.Forms;
    using Views;

    class LogInViewModel : BaseViewModel
    {
        
        #region Atributos
        private string email;
        private string password;
        private bool isRunning;
        private bool isRemembered;
        private bool isEnabled;
        #endregion

        #region Propiedades
        public string Email
        {
            get { return email; }
            set { SetValue(ref email, value); }
        }
        public string Password
        {
            get { return password; }
            set { SetValue(ref password, value);}
        }
        public bool IsRunning
        {
            get { return isRunning; }
            set { SetValue(ref isRunning, value); }
        }
                                
        public bool IsRemembered
        {
            get { return isRemembered; }
            set { SetValue(ref isRemembered, value); }
        }
        public bool IsEnabled
        {
            get { return isEnabled; }
            set { SetValue(ref isEnabled, value); }
        }

        #endregion

        #region Constructores
        public LogInViewModel()
        {
            this.IsRemembered = true;
            this.IsEnabled = true;
        }
        #endregion

        #region Comandos
        public ICommand LogInCommand
        { get { return new RelayCommand(LogIn); } }

        
        private async void LogIn()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert("Atencion de LogIn","Debes ingresar el Email","Aceptar");
                return;
            }
            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert("Atencion de LogIn", "Debes ingresar el Password", "Aceptar");
                return;
            }

            IsRunning = true;
            IsEnabled = false;
            if (this.Email != "fusuga@gmail.com" || this.Password != "1234")
            {
                await Application.Current.MainPage.DisplayAlert("Atencion de LogIn", "Email o Password Incorrectos", "Aceptar");
                this.Password = string.Empty;
                IsRunning = false;
                IsEnabled = true;
                return;
            }

            MainViewModel.GetInstance().Lands = new LandsViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new LandsPage());
            this.Email = string.Empty;
            this.Password = string.Empty;
            IsRunning = false;
            IsEnabled = true;

        }

        public ICommand RegisterCommand { get; set; }
        #endregion
    }
}
