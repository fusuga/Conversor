﻿
namespace Lands.ViewModels
{
    using System.Collections.Generic;
    using Models;   

    class MainViewModel
    {
        #region Atributos
        private static MainViewModel instance;
        #endregion

        #region Propiedades
        public LogInViewModel LogIn { get; set; }
        public LandsViewModel Lands { get; set; }
        public LandViewModel Land { get; set; }
        public List<Land> LandsList { get; set; }
        #endregion

        #region Constructores
        public MainViewModel()
        {
            instance = this;
            this.LogIn = new LogInViewModel();
        }
        #endregion

        #region Metodos
        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }
            return instance;
        }
        #endregion
    }
}
